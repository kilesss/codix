<?php 
//  1. Group countries by region
//  1.1. Find how many countries are in a given region
//  1.2. Sum people population in region
//  1.3. Print results ascending
//    1.3.1. by population
//    1.3.2. by counties



// 2. Group countries by language
//  2.1. Find how many people speak given language
//  2.3. Print results descending

class IndexController{

    private $restApiUrl = 'https://restcountries.eu/rest/v2/all';
    private $restData;

    public function index(){
        $this->getRestResponce();
    }

    private function getRestResponce(){
        $this->restData = $this->callAPI($this->restApiUrl);
        $data = $this->parceData();
        require_once ('view.php');

    }

    private function parceData(){
        $regions = [];
        $regionsPopulation = [];
        $populations = [];
        $languages = [];
        $languagesFormatedByPopulation = [];
        $languagesFormated = [];
        $regionsPopulationFormatted = [];
        $regionsCountriesFormatted = [];
        $regionsCountriesFormattedPopulation = [];

        foreach ($this->restData as $data){
            if($data->region != ''){
                if(isset($regions[$data->region])){
                    if(!in_array($data->name, $regions[$data->region])){
                        $regions[$data->region][$data->name] = $data->population;
                    }
                    $populations[$data->region] += $data->population;
                }else{
                    $regions[$data->region] = [];
                    $populations[$data->region] = $data->population;
                    $regions[$data->region][$data->name] = '';
               }
                foreach ($data->languages as $lang){
                    if(isset($languages[$lang->name])){
                        $languages[$lang->name] += $data->population;
                    }else{
                        $languages[$lang->name] = $data->population;
                    }
                }
            }
        }
        krsort($languages);
        foreach ($languages as $key => $val) {
            $languagesFormated[$key]= $val;
        }
        
        arsort($languages);
        foreach ($languages as $key => $val) {
            $languagesFormatedByPopulation[$key]= $val;
        }
        ksort($populations);
        foreach ($populations as $key => $val) {
            $regionsPopulationFormatted[$key]= $val;
        }


        ksort($regions);
        foreach ($regions as $key => $countries) {

            $countriesFormatted = [];      
            $countriesFormattedPopulation = [];      
    
            asort($countries);
            foreach ($countries as $countriesKey => $countriesVal) {
        
                $countriesFormattedPopulation[$countriesKey]= $countriesVal;
            }

            ksort($countries);
            foreach ($countries as $countriesKey => $countriesVal) {
        
                $countriesFormatted[$countriesKey]= $countriesVal;
            }

          
            $regionsCountriesFormattedPopulation[$key] = $countriesFormattedPopulation;
            $regionsCountriesFormatted[$key] = $countriesFormatted;
        
        }
            return [
                'regionCountries' => $regionsCountriesFormatted,
                'regionCountriesPopulation' => $regionsCountriesFormattedPopulation,
                'regionPopulation' => $regionsPopulationFormatted,
                'languagesByCountries' => $languagesFormated,
                'languagesByPopulation' => $languagesFormatedByPopulation,
            ];
    }
  
    private function callAPI($url){
       //next example will recieve all messages for specific conversation
        $curl = curl_init($this->restApiUrl);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $curl_response = curl_exec($curl);
        $response = [];
        if ($curl_response === false) {            
            curl_close($curl);
            return false;
        }
        curl_close($curl);
        return json_decode($curl_response);
     }

}